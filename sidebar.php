<?php

do_action( 'genesis_before_sidebar_widget_area' );
do_action( 'genesis_sidebar' );
do_action( 'genesis_after_sidebar_widget_area' );

if ( is_active_sidebar( 'sidebar-1' ) ) { ?>
	<div id="secondary" class="widget-area" role="complementary">
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</div><!-- #secondary -->
<?php } ?>