<?php
do_action( 'genesis_doctype' );
do_action( 'genesis_title' );
do_action( 'genesis_meta' );

wp_head(); ?>
</head>
<?php
genesis_markup( array(
    'html5'   => '<body %s>',
    'xhtml'   => sprintf( '<body class="%s">', implode( ' ', get_body_class() ) ),
    'context' => 'body',
) );
do_action( 'genesis_before' );
do_action( 'genesis_before_header' ); ?>

<header id="header">
    <div class="container">
        <div class="header-wrap">
            <div id="logo">
                <a href="<?php echo home_url(); ?>">
                    <img src="<?php echo CHILD_URL; ?>/images/header_logo_s.png" border="0" width="129px" class="img-responsive" />
                </a>
            </div>

            <nav id="primary-navbar" class="navbar navbar-default" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button data-target="#primary-navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <div id="primary-navbar-collapse" class="collapse navbar-collapse">
                        <?php
                            do_action( 'genesis_after_header' );
                        ?>
<!--                        <ul class="nav navbar-nav">
                            <li class="active">
                                <a href="#" class="active">
                                    <span>HOTEL</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>LAKE FRONT APARTMENTS</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>THE RESIDENCE</span>
                                </a>
                            </li>
                        </ul>

                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#">
                                    <span>EICHARDT'S BAR</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo home_url('about'); ?>">
                                    <span>ABOUT US</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>EVENTS</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>CONTACT US</span>
                                </a>
                            </li>
                        </ul>-->
                    </div>
                    <!-- .navbar-collapse -->
                </div>
                <!-- .container-fluid -->
            </nav>
        </div>
        <!-- .header-wrapper -->
    </div>
</header>
<!-- #header -->

<?php  ?>