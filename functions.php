<?php
//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', 'Eichardt Hotel' );
define( 'CHILD_THEME_URL', 'http://www.eichardts.com/' );
define( 'CHILD_THEME_VERSION', '0.1' );

//* Enqueue Lato Google font
add_action( 'wp_enqueue_scripts', 'genesis_sample_google_fonts' );
function genesis_sample_google_fonts() {
	wp_enqueue_style( 'google-font-lato', '//fonts.googleapis.com/css?family=Lato:300,700', array(), CHILD_THEME_VERSION );
}

//* Add HTML5 markup structure
add_theme_support( 'html5' );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Add support for custom background
add_theme_support( 'custom-background' );

//* Add support for 3-column footer widgets
add_theme_support( 'genesis-footer-widgets', 3 );

/*
 * ==============================================
 * CUSTOM CODE BEGINS HERE
 * ==============================================
 */

include_once( CHILD_DIR . '/lib/init.php' );
include_once( CHILD_DIR . '/lib/blog.php' );
include_once( CHILD_DIR . '/lib/page.php' );
include_once( CHILD_DIR . '/lib/widgets.php' );

// Load Custom Structure
require_once( CHILD_DIR . '/lib/structure/posts.php' );
require_once( CHILD_DIR . '/lib/structure/loops.php' );

remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'genesis_after_header', 'custom_do_nav' );
function custom_do_nav() {
    wp_nav_menu(array(
        'menu' => 'Home Left',
        'menu_class' => 'nav navbar-nav',
        'menu_id' => 'navigation',
        'link_before' => '<span>',
        'link_after'=>'</span>'
    ));
}
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_after_header', 'custom_do_subnav' );
function custom_do_subnav() {
    
    wp_nav_menu(array(
        'menu' => 'Home Right',
        'menu_class' => 'nav navbar-nav navbar-right',
        'menu_id' => 'navigation',
        'link_before' => '<span>',
        'link_after'=>'</span>'
    ));
}
add_filter('widget_text', 'do_shortcode');

add_filter('widget_title','my_widget_title',10,3);
function my_widget_title($title)
{
    $title = str_replace("[br]", "<br/>", $title);
    return $title;
}