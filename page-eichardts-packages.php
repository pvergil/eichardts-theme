<?php

// Add custom page widget function.
add_action( 'genesis_after_content_sidebar_wrap', 'EICHARD_page_bottom_packages' );

function EICHARD_page_bottom_packages() { ?>
    <div class="widgets widgets-bottom">
        <div class="container">
            <div class="row">
                <?php
                $_page = get_page_by_path('eichardts-packages');

                $posts = new WP_Query(array(
                    'post_type' => 'page',
                    'post_parent' => $_page->ID,
                    'posts_per_page' => 4
                ));

                if ( $posts->have_posts() ) {
                    while ( $posts->have_posts() ) {
                        $posts->the_post(); ?>
                        <div class="col-md-3">
                            <div class="column centered">
                                <h4><?php the_field('package_title'); ?><br />PACKAGE</h4>
                                <span class="price">$<?php the_field('price'); ?></span>
                                <footer>
                                    <a href="<?php echo get_post_permalink();?>" class="btn btn-sm btn-default btn-transparent">VIEW PACKAGE</a>
                                </footer>
                            </div>
                        </div>
                    <?php } // endwhile
                }

                /* Restore original Post Data */
                wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
    <!-- div.widgets.footer -->
<?php }

// Render the page.
EICHARD_page();