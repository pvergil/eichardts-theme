<?php do_action( 'genesis_before_footer' ); ?>
<div class="phone-footer">
 <div class="container">
   <div class="row">
   	<div class="contact_info">
     		<h6>EICHARDT’S HOTEL</h6>
        Marine Parade <br />
        Queenstown, 9348 <br />
        New Zealand <br /><br />
        P: +64 3 441 0450 <br />
        E: stay @eichardts .com 
 		</div>
 	</div>
</div>
</div>
<div class="footer">
<div class="row npad">
  	<div class="blue-background" style="background-color:#0d1f3a; position:absolute; width:30%; left:0; height:232px; z-index:-100;"></div>
    <div class="container">
            <div class="col-md-3 contact_container">
                <div class="contact_info">
                      <h6>EICHARDT’S HOTEL</h6>
                    	Marine Parade <br />
                    	Queenstown, 9348 <br />
                    	New Zealand <br /><br />
                    	P: +64 3 441 0450 <br />
                    	E: stay @eichardts .com 
                </div>
            </div>
            <div class="col-md-2">
                <div id="sitemap_info">
                    <div class="row">
                            <h6>Sitemap</h6>
                            <ul class="sitemap_list">
                              	<li>
                                    <a href="<?php echo home_url(); ?>">HOTEL</a>
                                </li>
                                <li>
                                    <a href="<?php echo home_url('lake-front-apartments') ?>">APARTMENTS</a>
                                </li>
                                <li>
                                    <a href="<?php echo home_url('the-residence'); ?>">THE RESIDENCE</a>
                                </li>
                                <li>
                                    <a href="#">EICHARDT'S BAR</a>
                                </li>
                                <li>
                                    <a href="<?php echo home_url('eichardts-packages'); ?>">PACKAGES</a>
                                </li>
                          
                            </ul>
                        </div>
                        
                </div>
            </div>
      			<div class="col-md-3">
                <div id="sitemap_info">
                    <div class="row">
                            <h6>SITEMAP</h6>
                            <ul class="sitemap_list">
                                <li><a href="#">GALLERY</a></li>
                                <li>
                                    <a href="<?php echo home_url('press'); ?>">PRESS</a>
                                </li>  
                                <li>
                                    <a href="<?php echo home_url('blog'); ?>">NEWS &amp; EVENTS</a>
                                </li>
                                <li>
                                    <a href="<?php echo home_url('contact-us'); ?>">CONTACT</a>
                                </li>
                            </ul>
                        </div>
                        
                </div>
            </div>
            <div class="col-md-4">
              	<div id="sitemap_info">
                    <div class="row">
                        <div class="col-md-12">
                            <h6>Newsletter Sign-Up</h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12"><?php gravity_form(2, false, false, false, '', true); ?></div>

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="copyright">© 2014 - EICHARDT’S HOTEL</p>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
</div>
<?php
do_action( 'genesis_after_footer' );
do_action( 'genesis_after' );
wp_footer();
?>
</body>
</html>
