<?php

// Add custom page widget function.
add_action( 'genesis_after_content_sidebar_wrap', 'EICHARD_page_bottom_widget' );

function EICHARD_page_bottom_widget() { ?>
    <div class="container">
        <div class="row">
            <?php dynamic_sidebar( 'contact-us-widget' ); ?>
        </div>
    </div>
<?php }

// Render the page.
EICHARD_contact_us_page();