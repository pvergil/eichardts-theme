<?php
//* Template Name: News


get_header();
?>
<header id="page-title">
    <h1><?php the_title(); ?></h1>
</header>
<div id="content" class="hfeed">
    <div class="container" id="news_page_container">
        <div class="row">
            <div class="col-lg-12">
                <?php
                    $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
                    $posts = new WP_Query(array(
                        'post_type' => 'post',
                        'category_name' => 'press',
                        'posts_per_page' => 6,
                        'paged' => $paged
                    ));

                    if ( $posts->have_posts() ) {
                        echo "<ul class='news_list'>";
                        while ( $posts->have_posts() ) {
                            $posts->the_post();
                        ?>
                            <li>
                                <div class="blist">
                                    <div class="left_p">
                                        <div class="blog_list_date_day">
                                            <div class="blog_list_date_day_caption text-center">
                                                <span><?php the_time('d'); ?></span>
                                            </div>
                                        </div>
                                        <div class="blog_list_info_year text-center">
                                            <?php the_time('F'); ?>
                                            <br />
                                            <span>
                                                <?php the_time('Y'); ?>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="left_r">
                                        <?php the_post_thumbnail('medium');?>
                                        <h4>
                                            <?php the_title(); ?>
                                        </h4>

                                        <?php the_excerpt(); ?>

                                        <a href="<?php the_permalink(); ?>" class="btn btn-sm btn-default btn-transparent">
                                            <?php _e('VIEW POST'); ?> <i class="icon icon-chevron-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </li>
                        <?php
                        }
                        echo "</ul>";
                        ?>
                        <?php

                        // get_next_posts_link() usage with max_num_pages
                        echo get_next_posts_link( 'Prev', $posts->max_num_pages );
                        echo get_previous_posts_link( 'Next' );
                        ?>

                    <?php
                    } else {
                        // no posts found
                    }

                    /* Restore original Post Data */
                    wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
</div>

<div class="widgets widgets-bottom">
    <div class="container">
        <div class="row">
            <?php dynamic_sidebar( 'news-widget' ); ?>
        </div>
    </div>
</div>

<?php get_footer();

?>