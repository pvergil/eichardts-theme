<?php
//* Template Name: Press


get_header();
?>
<header id="page-title">
    <h1><?php the_title(); ?></h1>
</header>
<div id="content" class="hfeed">
    <div class="container" id="press_page_container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <?php
                    $posts = new WP_Query(array(
                        'post_type' => 'post',
                        'category_name' => 'press'
                    ));

                    if ( $posts->have_posts() ) {
                        echo "<ul class='press_list'>";
                        while ( $posts->have_posts() ) {
                            $posts->the_post();
                        ?>
                            <div class="col-lg-3">
                                <div class='press_list_container text-center'>
                                    <a href="<?php echo get_post_permalink();?>">
                                    <?php

                                    the_post_thumbnail( 'press-thumbnail' );

                                    ?>
                                    <div class='caption'>
                                        <h4><?php the_title()?></h4>
                                        <p><span class="press_date"><?php echo get_the_date();?></span></p>
                                    </div>
                                    </a>
                                </div>
                            </div>
                        <?php
                        }
                        echo "</ul>";
                    } else {
                        // no posts found
                    }

                    /* Restore original Post Data */
                    wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="press_releases">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4>NEW PRESS & RELEASES</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-lg-12">
                <div class="row">
                    <?php
                    $posts = new WP_Query(array(
                        'post_type' => 'post', 
                        'post_status' => 'publish',
                        'cat' => 2,
                        'posts_per_page' => 3
                    ));

                    if ( $posts->have_posts() ) {
                            
                        while ( $posts->have_posts() ) {
                                $posts->the_post(); ?>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 blist_container">
                                <div class="blist">
                                    <div class="left_p">
                                        <div class="blog_list_date_day">
                                            <div class="blog_list_date_day_caption text-center">
                                                <span><?php the_time('d'); ?></span>
                                            </div>
                                        </div>
                                        <div class="blog_list_info_year text-center">
                                            <?php the_time('F'); ?>
                                            <br />
                                            <span>
                                                <?php the_time('Y'); ?>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="left_r">
                                        <h5>
                                            <?php the_title(); ?>
                                        </h5>

                                        <?php the_excerpt(); ?>

                                        <a href="<?php the_permalink(); ?>" class="btn btn-sm btn-default btn-transparent">
                                            <?php _e('VIEW POST'); ?> <i class="icon icon-chevron-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php }
                            
                    } else {
                        // no posts found
                    }

                    /* Restore original Post Data */
                    wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer();

?>