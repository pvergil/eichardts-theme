<?php

// Add custom page widget function.
add_action( 'genesis_after_content_sidebar_wrap', 'EICHARD_page_bottom_widget' );

function EICHARD_page_bottom_widget() { ?>
    <div class="widgets widgets-bottom">
        <div class="container">
            <div class="row">
                <?php dynamic_sidebar( 'footer-1' ); ?>
            </div>
        </div>
    </div>
    <!-- div.widgets.footer -->
<?php }

// Render the page.
EICHARD_page();