<?php get_header(); ?>
<div id="banner_slider" class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?php putRevSlider("Testslider","homepage") ?>
        </div>
    </div>
</div>

<div class="accomodation">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 28px;">
                <h4>
                    <?php _e('LUXURY ACCOMODATION'); ?>
                </h4>
                <p style="margin-bottom: 15px;">The stunning Queenstown hotel, offers accomodation in five luxurious suites with an additional four lake front apartments. The stunning Queenstown hotel, offers accomodation in five luxurious suites with an additional four lake front apartments. The stunning Queenstown hotel, offers accomodation in five luxurious suites with an additional four lake front apartments.</p>
                <a href="http://103.225.186.51/~eichardt/the-residence-2/" class="btn btn-default btn-transparent">
                    <?php _e('TAKE A TOUR OF OUR ROOMS'); ?> <i class="icon icon-chevron-right"></i>
                </a>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <?php echo do_shortcode( '[gravityform id="3" name="Booking Enquiry" title="false" description="false"]' ) ?>
            </div>
        </div>
    </div>
</div>
<!-- .accomodation -->

<div id="gallery">
<!--  	<div class="">
      <div class="row">
            <div class="col-md-6">
             </div>
        		<div class="col-md-6" style="background-color:#4e5769;">
             </div>
  	</div>-->
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="gallery_view">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="pull-left"><h4>GALLERY</h4></div>
                        </div>
                        <div class="col-md-6">
                            <div class="pull-right gallery_view_more">
                                <a href="#" class="btn_view_more">
                                    See More  <img src="<?php echo CHILD_URL; ?>/images/eichardts-arrow.png" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-lg-12 gallery_image_container">
                            <div class="row">
                                <?php echo do_shortcode( '[thumb_scroller name="Homepage"]' ) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="gallery_preview">
                    <div class="gallery_preview_inner">
                        <div class="row">
                            <div class="col-xs-5 col-sm-6 col-lg-6">
                                <div class="gallery_preview_content">
                                    <h4 style="color: #fff; text-transform: uppercase;">Eichardt’s BAR</h4>
                                    <p style="color: #fff;">Eichardt’s renowned Bar, with its comfortable sofas, cosy fireplace and gracious service, provides a welcoming and stylish environment for sampling mouth-watering creations</p>
                                    <a href="#" class="btn btn-sm btn-default btn-inverse btn-transparent">
                                        <?php _e('SEE MORE'); ?> <i class="icon icon-chevron-right"></i>
                                    </a> 
                                </div>
                            </div>
                            <div class="col-xs-7 col-sm-6 col-lg-6">
                                <img src="<?php echo CHILD_URL; ?>/images/eichardts-bar-homepage.jpg" class="img-responsive" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- .gallery -->

<div class="press_releases">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4>LATEST NEWS</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-lg-12">
                <div class="row">
                    <?php
                    $posts = new WP_Query(array(
                        'post_type' => 'post', 'posts_per_page' => 3, 'cat' => 2
                    ));

                    if ( $posts->have_posts() ) {
                            
                        while ( $posts->have_posts() ) {
                                $posts->the_post(); ?>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 blist_container">
                                <div class="blist">
                                    <div class="left_p">
                                        <div class="blog_list_date_day">
                                            <div class="blog_list_date_day_caption text-center">
                                                <span><?php the_time('d'); ?></span>
                                            </div>
                                        </div>
                                        <div class="blog_list_info_year text-center">
                                            <?php the_time('F'); ?>
                                            <br />
                                            <span>
                                                <?php the_time('Y'); ?>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="left_r">
                                        <h5>
                                            <?php the_title(); ?>
                                        </h5>

                                        <?php the_excerpt(); ?>

                                        <a href="<?php the_permalink(); ?>" class="btn btn-sm btn-default btn-transparent">
                                            <?php _e('VIEW POST'); ?> <i class="icon icon-chevron-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php }
                            
                    } else {
                        // no posts found
                    }

                    /* Restore original Post Data */
                    wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- .press_releases -->

<?php get_footer(); ?>