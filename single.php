<?php

////////////////////////////////////////
// Display the post title just below the header.
add_action( 'genesis_before_content', 'EICHARD_single_title' );

function EICHARD_single_title() { ?>
    <header id="page-title">
        <h1><?php the_title(); ?></h1>
    </header>
<?php }

////////////////////////////////////////
// Display the pagination links.
add_action( 'genesis_before_content', 'EICHARD_single_pagination' );

function EICHARD_single_pagination() { ?>
    <div class="post-pagination">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="post-navigation pull-left">
                        <li class="prev pull-left">
                            <a href="<?php echo home_url('blog'); ?>"><?php _e('Back to posts page'); ?></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="post-navigation pull-right">
                        <?php if (get_previous_post_link()) { ?>
                            <li class="prev pull-left"><?php previous_post_link('%link', 'prev post'); ?></li>
                        <?php } ?>

                        <?php if (get_previous_post_link() && get_next_post_link()) { ?>
                            <li class="separator pull-left">|</li>
                        <?php } ?>

                        <?php if (get_next_post_link()) { ?>
                            <li class="next pull-left"><?php next_post_link('%link', 'next post'); ?></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
<?php }

////////////////////////////////////////
// Wrap the entire content in a Bootstrap container.
add_action( 'genesis_before_loop', 'EICHARD_single_before_loop' );

function EICHARD_single_before_loop() { ?>
    <div class="container">
        <div class="row">
<?php }

add_action( 'genesis_after_loop', 'EICHARD_single_after_loop' );

function EICHARD_single_after_loop() { ?>
        </div> 
        <!-- div.row -->
    </div>
    <!-- div.container -->
<?php }

////////////////////////////////////////
// Wrap the blog post in a Bootstrap grid column.
add_action( 'genesis_before_entry', 'EICHARD_single_before_entry' );

function EICHARD_single_before_entry() { ?>
    <!-- blog column -->
    <div class="col-md-8">
<?php }

add_action( 'genesis_after_entry', 'EICHARD_single_after_entry' );

function EICHARD_single_after_entry() { ?>
    </div>
    <!-- blog column -->
<?php }

////////////////////////////////////////
// Display the post title and author inside the content area.
add_action( 'genesis_entry_header', 'EICHARD_single_post_header' );

function EICHARD_single_post_header() { ?>
    <header class="post-header">
        <h4 class="post-title"><?php the_title(); ?></h4>
        <span class="post-author">
            <?php _e('Posted by'); ?> <?php the_author(); ?>
        </span>
    </header>
<?php }

////////////////////////////////////////
// Display the featured image for this post if available.
add_action( 'genesis_entry_header', 'EICHARD_single_post_featured' );

function EICHARD_single_post_featured() { ?>
    <div class="post-featured">
        <?php
        if ( has_post_thumbnail() ) {
            the_post_thumbnail('large', array('class' => 'img-responsive entry-image'));
        } // endif ?>
    </div>
<?php }

////////////////////////////////////////
// Add the sidebar.
add_action( 'genesis_after_endwhile', 'EICHARD_single_sidebar' );

function EICHARD_single_sidebar() { ?>
    <!-- blog sidebar -->
    <div class="col-md-4">
        <?php get_sidebar(); ?>
    </div>
    <!-- blog sidebar -->
<?php }

////////////////////////////////////////
// Add the bottom widgets.
add_action( 'genesis_after_content', 'EICHARD_single_widgets' );

function EICHARD_single_widgets() { ?>
    <div class="widgets widgets-bottom">
        <div class="container">
            <div class="row">
                <?php dynamic_sidebar( 'news-widget' ); ?>
            </div>
        </div>
    </div>
<?php }

////////////////////////////////////////
// Remove the post meta.
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

////////////////////////////////////////
// Remove the post footer.
remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_open', 5 );
remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_close', 15 );

////////////////////////////////////////
// Remove the comment form.
remove_action( 'genesis_after_entry', 'genesis_get_comments_template' );

genesis();