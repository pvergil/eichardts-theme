<?php
// All custom blog functions can be found in lib/structure/posts.php

// Disable default post info function.
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );

// Replace it with a custom post thumbnail function.
add_action( 'genesis_entry_header', 'EICHARD_blog_image', 4 );

// Disable default post title function.
remove_action( 'genesis_entry_header', 'genesis_do_post_title' );

// Add custom post title function.
add_action( 'genesis_entry_header', 'EICHARD_blog_title' );

// Create a custom query.
add_action( 'genesis_before_content', 'EICHARD_blog_before_content' );

function EICHARD_blog_before_content() {
    $include = genesis_get_option( 'blog_cat' );
    $exclude = genesis_get_option( 'blog_cat_exclude' ) ? explode( ',', str_replace( ' ', '', genesis_get_option( 'blog_cat_exclude' ) ) ) : '';
    $paged   = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

    $args = wp_parse_args(
        genesis_get_custom_field( 'query_args' ),
        array(
            'cat'              => $include,
            'category__not_in' => $exclude,
            'showposts'        => genesis_get_option( 'blog_cat_num' ),
            'paged'            => $paged,
        )
    );

    query_posts($args);
}

// Restore original query.
add_action( 'genesis_after_content', 'EICHARD_blog_after_content' );

function EICHARD_blog_after_content() {
    wp_reset_query();
}

// Disable default loop function.
remove_action( 'genesis_loop', 'genesis_do_loop' );

// Add custom loop function.
add_action( 'genesis_loop', 'EICHARD_blog_loop' );

// Disable default post content function.
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

// Add custom post content function.
add_action( 'genesis_entry_content', 'EICHARD_blog_content' );

// Disable default post meta function.
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

// Replace it with a custom "View Post" button.
add_action( 'genesis_entry_footer', 'EICHARD_blog_link' );

// Disable pagination at the bottom of the page.
remove_action( 'genesis_after_endwhile', 'genesis_posts_nav' );

// Add custom opening and closing wrapper for each blog posts.
add_action( 'genesis_before_entry', 'EICHARD_blog_before_entry' );
add_action( 'genesis_after_entry', 'EICHARD_blog_after_entry' );

// Add custom excerpt length.
add_filter('excerpt_length', 'EICHARD_blog_excerpt_length', 999);

// Add the blog widgets.
add_action( 'genesis_after_content_sidebar_wrap', 'EICHARD_page_bottom_widget' );

function EICHARD_page_bottom_widget() { ?>
    <div class="widgets widgets-bottom">
        <div class="container">
            <div class="row">
                <?php dynamic_sidebar( 'news-widget' ); ?>
            </div>
        </div>
    </div>
<?php }

// Don't display the sidebar on the blog page.
remove_action( 'genesis_sidebar', 'genesis_do_sidebar' );

// Render the blog posts.
EICHARD_blog();