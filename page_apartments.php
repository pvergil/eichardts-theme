<?php
//* Template Name: Apartments


get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>
<?php the_content(); ?>
<?php endwhile; endif; ?>

<!-- .accomodation -->
<div id="gallery">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="gallery_view_apartment" style="padding:40px 0px;">
                    <div class="row">
                        
                           
                               <?php echo do_shortcode( '[thumb_scroller name="Apartments"]' ) ?>
                               
                            </div>
                        </div>
                    </div>
                       <div class="col-md-4">
                <div class="gallery_preview">
                    <div class="gallery_preview_inner">
                        <div class="row">
                            <div class="col-lg-9">
                                <div class="gallery_preview_content">
                                    <h4 style="color: #fff; text-transform: uppercase;">Exclusive Use</h4>
                                    <p>Eichardt’s Lorem ipsum dolor sit amet, consectetur 
                                    adipiscing elit. Phasellus auctor faucibus mauris 
                                    non imperdiet.</p>
                                    <a href="#" class="btn btn-sm btn-default btn-inverse btn-transparent">
                                        <?php _e('SEE MORE'); ?> <i class="icon icon-chevron-right"></i>
                                    </a> 
                                </div>
                            </div>
                        </div>
                    </div>
      
      
      </div>
            </div>
     
                </div>
            </div>
        </div>

<!-- .gallery -->
<div class="widgets footer">
    <div class="container">
        <div class="row">
            <?php dynamic_sidebar( 'news-widget' ); ?>
        </div>
    </div>
</div>
<?php get_footer();

?>