<?php

/*
 * Override the default genesis implementations.
 */
remove_action( 'genesis_meta', 'genesis_load_stylesheet' );

function EICHARD_init() {
    wp_register_style(
        'EICHARD_bootstrap', CHILD_URL . '/styles/bootstrap.min.css', false, '3.1.0'
    );

    wp_register_script(
        'EICHARD_bootstrap', CHILD_URL . '/scripts/bootstrap.min.js', array('jquery'), '3.1.0', true
    );

    wp_register_style(
        'EICHARD_overrides', CHILD_URL . '/styles/overrides.css', array('EICHARD_bootstrap')
    );
}

add_action( 'init', 'EICHARD_init' );

function EICHARD_styles() {
    if ( !is_admin() ) {
        wp_enqueue_style( 'EICHARD_style', get_stylesheet_uri(), array('EICHARD_overrides'), CHILD_THEME_VERSION );
    }
}

add_action('wp_enqueue_scripts', 'EICHARD_styles', 5);

function EICHARD_scripts() {
    if ( !is_admin() ) {
        wp_enqueue_script('EICHARD_bootstrap');
    }
}

add_action('wp_enqueue_scripts', 'EICHARD_scripts');

function EICHARD_excerpt_length( $length ) {
    return 20;
}

add_filter('excerpt_length', 'EICHARD_excerpt_length', 999);

function EICHARD_excerpt_more( $more ) {
    return '...';
}
add_filter('excerpt_more', 'EICHARD_excerpt_more');

// Add custom image sizes
add_image_size( 'press-thumbnail', 300, 300, TRUE );