<?php

// Remove the default footer widget areas function.
remove_action( 'after_setup_theme', 'genesis_register_footer_widget_areas' );

// Add a custom footer widget areas function.
add_action( 'after_setup_theme', 'EICHARD_footer_widget_areas' );

function EICHARD_footer_widget_areas() {

    $footer_widgets = get_theme_support( 'genesis-footer-widgets' );

    if ( ! $footer_widgets || ! isset( $footer_widgets[0] ) || ! is_numeric( $footer_widgets[0] ) )
        return;

    $footer_widgets = (int) $footer_widgets[0];

    $counter = 1;

    while ( $counter <= $footer_widgets ) {
        genesis_register_sidebar(
            array(
                'id'               => sprintf( 'footer-%d', $counter ),
                'name'             => sprintf( __( 'Footer %d', 'genesis' ), $counter ),
                'description'      => sprintf( __( 'Footer %d widget area.', 'genesis' ), $counter ),
                '_genesis_builtin' => true,
                'before_widget'    => '<div id="%1$s" class="col-md-4">',
                'after_widget'     => '</div>',
                'before_title'     => '<header class="narrow centered"><h4>',
                'after_title'      => '</h4></header>',
            )
        );

        $counter++;
    }

    genesis_register_sidebar(
        array(
            'id'               => 'pamper-package-widget',
            'name'             => __( 'Pamper Package Widgets', 'genesis' ),
            'description'      => __( 'The pamper package widget area.', 'genesis' ),
            'before_widget'    => '<div id="%1$s" class="col-md-3"><div class="column centered">',
            'after_widget'     => '</div></div>',
            'before_title'     => '<header><h4>',
            'after_title'      => '</h4></header>',
        )
    );
	
    genesis_register_sidebar(
        array(
            'id'               => 'news-widget',
            'name'             => __( 'News Widgets', 'genesis' ),
            'description'      => __( 'The news widget area.', 'genesis' ),
            'before_widget'    => '<div id="%1$s" class="col-md-4"><div class="column centered narrow ">',
            'after_widget'     => '</div></div>',
            'before_title'     => '<header><h4>',
            'after_title'      => '</h4></header>',
        )
    );
    genesis_register_sidebar(
        array(
            'id'               => 'contact-us-widget',
            'name'             => __( 'Contact Us Widgets', 'genesis' ),
            'description'      => __( 'The Contact Us widget area.', 'genesis' ),
            'before_widget'    => '<div id="%1$s" class="col-md-4"><div class="column centered narrow ">',
            'after_widget'     => '</div></div>',
            'before_title'     => '<header><h4>',
            'after_title'      => '</h4></header>',
        )
    );
}

// Remove the default footer widgets function.
remove_action( 'genesis_before_footer', 'genesis_footer_widget_areas' );