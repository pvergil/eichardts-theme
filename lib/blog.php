<?php

function EICHARD_blog() {
    get_header();

    do_action( 'genesis_before_content_sidebar_wrap' ); ?>

    <section id="content-sidebar-wrap">
        <header id="page-title">
            <h1><?php _e('News'); ?></h1>
        </header>

        <?php do_action( 'genesis_before_content' ); ?>

        <div id="content" class="hfeed">
            <div class="container">
                <header>
                    <div class="row">
                        <div class="col-md-6">
                            &nbsp;
                        </div>
                        <div class="col-md-6">
                            <?php EICHARD_blog_pagination(); ?>
                        </div>
                    </div>
                </header>

                <div class="row">
                    <?php
                    do_action( 'genesis_before_loop' );
                    do_action( 'genesis_loop' );
                    do_action( 'genesis_after_loop' );
                    ?>
                </div>

                <footer>
                    <div class="row">
                        <div class="col-md-6">
                            &nbsp;
                        </div>
                        <div class="col-md-6">
                            <?php EICHARD_blog_pagination(); ?>
                        </div>
                    </div>
                </footer>

            </div>
        </div>
        <!-- #content -->

        <?php do_action( 'genesis_after_content' ); ?>

    </section>
    <!-- #content-sidebar-wrap -->

    <?php do_action( 'genesis_after_content_sidebar_wrap' ); ?>

    <?php get_footer();
}