<?php

// Disable default entry header markups.
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 5 );
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 15 );

// Disable default page title function.
remove_action( 'genesis_entry_header', 'genesis_do_post_title' );

// Add custom page title function.
add_action( 'genesis_before_content', 'EICHARD_page_title' );

function EICHARD_page_title() {
    if ( is_page() ) { ?>
        <header id="page-title">
            <div class="container">
                <h1><?php the_title(); ?></h1>

                <?php if ( get_field('subheading') ) { ?>
                    <h5><?php the_field('subheading'); ?></h5>
                <?php } ?>
            </div>
        </header>
    <?php } // endif
}

function EICHARD_page() {
    get_header();

    do_action( 'genesis_before_content_sidebar_wrap' ); ?>

    <section id="content-sidebar-wrap">
        <?php do_action( 'genesis_before_content' ); ?>

        <div id="content" class="hfeed">
            <?php

            do_action( 'genesis_before_loop' );
            do_action( 'genesis_loop' );
            do_action( 'genesis_after_loop' );

            ?>
        </div>
        <!-- #content -->

        <?php do_action( 'genesis_after_content' ); ?>
    </section>
    <!-- #content-sidebar-wrap -->

    <?php do_action( 'genesis_after_content_sidebar_wrap' ); ?>

    <?php get_footer();
}

function EICHARD_contact_us_page() {
    get_header();

    do_action( 'genesis_before_content_sidebar_wrap' ); ?>

    <section id="content-sidebar-wrap">
        <?php do_action( 'genesis_before_content' ); ?>

        <div id="content" class="pfeed contact_us_content">
            <?php

            do_action( 'genesis_before_loop' );
            do_action( 'genesis_loop' );
            do_action( 'genesis_after_loop' );

            ?>
        </div>
        <!-- #content -->

        <?php do_action( 'genesis_after_content' ); ?>
    </section>
    <!-- #content-sidebar-wrap -->

    <?php do_action( 'genesis_after_content_sidebar_wrap' ); ?>

    <?php get_footer();
}