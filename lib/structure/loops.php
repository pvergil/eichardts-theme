<?php

function EICHARD_blog_loop() {

    if ( ! genesis_html5() ) {
        genesis_legacy_loop();
        return;
    }
    
    if ( have_posts() ) {
        while ( have_posts() ) { the_post(); ?>

            <?php do_action( 'genesis_before_entry' ); ?>

            <article <?php echo genesis_attr('entry'); ?>>
                <div class="row">
                    <div class="col-xs-2 col-sm-2 col-md-2">
                        <div class="post-time">
                            <div class="dd">
                                <?php the_time('d'); ?>
                            </div>
                            <div class="mm">
                                <?php the_time('F'); ?>
                            </div>
                            <div class="yy">
                                <?php the_time('Y'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-10 col-sm-10 col-md-10">
                        <?php
                        do_action( 'genesis_entry_header' );
                        do_action( 'genesis_before_entry_content' );
                        ?>

                        <div <?php echo genesis_attr( 'entry-content' ); ?>>
                            <?php do_action( 'genesis_entry_content' ); ?>
                        </div>

                        <?php
                        do_action( 'genesis_after_entry_content' );
                        do_action( 'genesis_entry_footer' );
                        ?>
                    </div>
                </div>
            </article>

            <?php do_action( 'genesis_after_entry' ); ?>

        <?php } // endwhile

        do_action( 'genesis_after_endwhile' );

    } else {
        do_action( 'genesis_loop_else' );
    } // endif

}