<?php

function EICHARD_blog_title() {

    $title = apply_filters( 'genesis_post_title_text', get_the_title() );

    if ( 0 === mb_strlen( $title ) )
        return;

    //* Wrap in H1 on singular pages
    $wrap = is_singular() ? 'h1' : 'h2';

    //* Also, if HTML5 with semantic headings, wrap in H1
    $wrap = genesis_html5() && genesis_get_seo_option( 'semantic_headings' ) ? 'h1' : $wrap;

    //* Build the output
    $output = genesis_markup( array(
        'html5'   => "<{$wrap} %s>",
        'xhtml'   => sprintf( '<%s class="entry-title">%s</%s>', $wrap, $title, $wrap ),
        'context' => 'entry-title',
        'echo'    => false,
    ) );

    $output .= genesis_html5() ? "{$title}</{$wrap}>" : '';

    echo apply_filters( 'genesis_post_title_output', "$output \n" );

}

function EICHARD_blog_image() {
    if ( has_post_thumbnail() ) {
        the_post_thumbnail('large', array('class' => 'img-responsive entry-image'));
    } else {
        printf('<img class="img-responsive entry-image" src="%s/images/blog-default.jpg">', CHILD_URL);
    }
}

function EICHARD_blog_content() {
    the_excerpt();
}

function EICHARD_blog_link() { ?>
    <a href="<?php the_permalink(); ?>" class="btn btn-sm btn-default">
        <?php _e('VIEW POST'); ?> <i class="icon icon-chevron-right"></i>
    </a> 
<?php }

function EICHARD_blog_pagination() {
    // Don't print empty markup if there's only one page.
    if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
        return;
    } ?>

    <div class="post-pagination">
        <div class="pull-right">
            <div class="pull-left label">
                <?php _e('Pages:'); ?>
            </div>
            <div class="pull-left links">
                <?php
                if ( 'numeric' === genesis_get_option( 'posts_nav' ) )
                    EICHARD_blog_numeric_pagination();
                else
                    genesis_prev_next_posts_nav();
                ?>
            </div>
        </div>
    </div>
<?php }

function EICHARD_blog_numeric_pagination() {

    if( is_singular() )
        return;

    global $wp_query;

    //* Stop execution if there's only 1 page
    if( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    //* Add current page to the array
    if ( $paged >= 1 )
        $links[] = $paged;

    //* Add the pages around the current page to the array
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<ul class="pagination">';

    //* Link to first page, plus ellipses if necessary
    if ( ! in_array( 1, $links ) ) {

        $class = 1 == $paged ? ' class="active"' : '';

        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

        if ( ! in_array( 2, $links ) )
            echo '<li class="pagination-omission">&#x02026;</li>';

    }

    //* Link to current page, plus 2 pages in either direction if necessary
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }

    //* Link to last page, plus ellipses if necessary
    if ( ! in_array( $max, $links ) ) {

        if ( ! in_array( $max - 1, $links ) )
            echo '<li class="pagination-omission">&#x02026;</li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );

    }

    echo '</ul>' . "\n";

}

function EICHARD_blog_excerpt_length( $length ) {
    return 100;
}

function EICHARD_blog_before_entry() {
    echo '<div class="col-md-6">';
}

function EICHARD_blog_after_entry() {
    echo '</div>';
}